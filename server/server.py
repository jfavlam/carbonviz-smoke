#!/usr/bin/env python

from flask import Flask, request, json, jsonify
import time
import RPi.GPIO as GPIO
from PCA9685 import PCA9685

PORT = 5000
DEBUG = False
# Press duration in second
DEFAULT_PRESS_DURATION = 0.2
# Map data value to press duration
MIN_PRESS_DURATION = 0.2
MAX_PRESS_DURATION = 2.5
# Servo angle
START_ANGLE = 2
END_ANGLE = 40

# Instantiate the app
app = Flask(__name__)

# ##########################################################
# /
# ##########################################################
@app.route("/")
def hello_world():
    return "<h1>CarbonViz Smoke 💨💨💨</h1>"

# ##########################################################
# /servo
# ##########################################################
@app.route('/servo', methods=['POST'])
def process_json():
    # content_type = request.headers.get('Content-Type')
    # print(content_type)

    data = json.loads(request.data)

    count = data['count']
    newData = data['newData']
    oldData = data['oldData']
    diffData = newData - oldData
    newCo2 = data['newCo2']
    oldCo2 = data['oldCo2']
    diffCo2 = float(newCo2) - float(oldCo2)
    diffCo2 = format(diffCo2, '.2f')
    
    print("")
    print ("Count: " + str(count))
    print ("New Data: " + str(newData))
    print ("Old Data: " + str(oldData))
    print ("Diff Data: " + str(diffData))
    print ("New CO2: " + str(newCo2))
    print ("Old CO2: " + str(oldCo2))
    print ("Diff CO2: " + str(diffCo2))
    print ("")

    pressDuration = DEFAULT_PRESS_DURATION

    if(diffData > 0):
        pressDuration = map(diffData, 1000, 500000, MIN_PRESS_DURATION, MAX_PRESS_DURATION)

    start_servo(pressDuration)

    return 'Trigger servo! Press button during ' + str(format(pressDuration, '.2f')) + ' sec.'

    #  response_object = {'status': 'success'}
    # if (content_type == 'application/json'): # request.is_json
    #     # json = request.json
    #     jsonDic = request.get_json()
        
    #     # print(json)
    #     print ("Count:" + jsonDic['count'])
    #     print ("New Data:" + jsonDic['newData'])
    #     print ("Old Data:" + jsonDic['oldData'])
    #     print ("New CO2:" + jsonDic['newCo2'])
    #     print ("Old CO2:" + jsonDic['oldCo2'])
    #     print ("")

    #     start_servo(1)
        
    #     return jsonify(response_object)
    # else:
    #     return 'Content-Type not supported!'

# Pass duration for button press (in second)
def start_servo(duration):
    count = 0

    try:
        pwm = PCA9685()
        pwm.setPWMFreq(50)
        #pwm.setServoPulse(1,500) 
        pwm.setRotationAngle(1, START_ANGLE)
        
        while True:
            count += 1

            print ("")

            print("Servo move start")

            pwm.setRotationAngle(1, START_ANGLE)
            print("Press button during " + str(duration) + " sec.")
            print("From " + str(START_ANGLE) + "° to " + str(END_ANGLE) + "°")
            time.sleep(duration)
            pwm.setRotationAngle(1, END_ANGLE)
            time.sleep(0.25)

            print("Servo move end")
            print ("")

            if count >= 1:
                break
                
    except:
        pwm.exit_PCA9685()
        print("\nTest program end. Bye! 👋")
        exit()

def map(val, inMin, inMax, outMin, outMax):
    return outMin + (outMax - outMin) * (val - inMin) / (inMax - inMin)

if __name__ == "__main__":
    app.run(debug=DEBUG, port=PORT)
