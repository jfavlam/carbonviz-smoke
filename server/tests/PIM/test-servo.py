import time
import pantilthat

# Press duration in second
DEFAULT_PRESS_DURATION = 2
# Servo angle
START_ANGLE = -50
END_ANGLE = -90

def start_servo(duration):
    count = 0

    pantilthat.idle_timeout(0.1)

    while True:
        count += 1

        print ("")
        print("Press button during " + str(duration) + " sec.")

        pantilthat.tilt(START_ANGLE)
        time.sleep(duration)
        pantilthat.tilt(END_ANGLE)
        time.sleep(0.25)

        print("Servo move end")
        print ("")

        if count >= 1:
            break

start_servo(DEFAULT_PRESS_DURATION)