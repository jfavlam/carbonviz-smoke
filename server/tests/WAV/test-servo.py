#!/usr/bin/python
import time
import RPi.GPIO as GPIO
from PCA9685 import PCA9685

# Press duration in second
DEFAULT_PRESS_DURATION = 2
# Servo angle
START_ANGLE = 2
END_ANGLE = 40

def start_servo(duration):
    count = 0

    try:
        pwm = PCA9685()
        pwm.setPWMFreq(50)
        #pwm.setServoPulse(1,500) 
        pwm.setRotationAngle(1, START_ANGLE)
        
        while True:
            count += 1

            print ("")

            print("Servo move start")

            pwm.setRotationAngle(1, START_ANGLE)
            print("Press button during " + str(duration) + " sec.")
            print("From " + str(START_ANGLE) + "° to " + str(END_ANGLE) + "°")
            time.sleep(duration)
            pwm.setRotationAngle(1, END_ANGLE)
            time.sleep(0.25)

            print("Servo move end")
            print ("")

            if count >= 1:
                exit()
                # break

    except:
        pwm.exit_PCA9685()
        print("\nTest program end. Bye! 👋")
        exit()

start_servo(DEFAULT_PRESS_DURATION)
