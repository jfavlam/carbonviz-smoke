alias dev='cd ~/DEV'

# CarbonViz Smoke
alias smo='cd ~/DEV/carbonviz-smoke'
alias smof='cd ~/DEV/carbonviz-smoke/webpack' # Frontend
alias smob='cd ~/DEV/carbonviz-smoke/server' # Backend
alias smos='smob && ./start.sh' # Run server 