Webpack Vue3 integration:

```
cd webpack/
npm install
```

Then, for dev env:

npm run watch

And for production build

npm run build

---

Run server:
`cd server`
`./start.sh`

Test POST request to server:

`curl -X POST -d '{"count": "0", "newData": "1", "oldData": "2", "newCo2": "3", "oldCo2": "4"}' -H 'content-type: application/json' http://localhost:5000/servo`

Test servo and adjusting angles:
`python3 test-servo.py`

---

## Hardware

- Raspberry Pi 3/4 + Pan-Tilt HAT
