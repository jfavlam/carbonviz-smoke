export const days = ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"];

export function roundToPrecision(val, precision = 2) {
  return parseFloat(val.toFixed(precision).toString());
}

export function formatSize(size, precision = 2) {
  if (size < 1000) {
    return `${roundToPrecision(size, precision)} B`;
  } else if (size < 1000000) {
    return `${roundToPrecision(size / 1000, precision)} kB`;
  } else if (size < 1000000000) {
    return `${roundToPrecision(size / 1000000, precision)} MB`;
  } else {
    return `${roundToPrecision(size / 1000000000, precision)} GB`;
  }
}

export function formatCo2(amount, precision = 2, unit = true) {
  if (amount < 1e-3) {
    if (unit) return `${roundToPrecision(amount * 1e6, precision)} mg`;
    return `${roundToPrecision(amount * 1e6, precision)}`;
  } else if (amount < 1) {
    if (unit) return `${roundToPrecision(amount * 1e3, precision)} g`;
    else return `${roundToPrecision(amount * 1e3, precision)}`;
  } else if (amount < 1000) {
    if (unit) return `${roundToPrecision(amount, precision)} kg`;
    else return `${roundToPrecision(amount, precision)}`;
  } else {
    if (unit) return `${roundToPrecision(amount / 1000, precision)} Mg`;
    else return `${roundToPrecision(amount / 1000, precision)}`;
  }
}
